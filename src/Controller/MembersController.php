<?php

namespace App\Controller;

use App\Entity\Members;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MembersController extends AbstractController
{
    /**
     * @Route("/api/members", name="members")
     */
    public function getAllMembers(){
        $members = $this->getDoctrine()->getRepository(Members::class)->findAll();
        return $this->json($members);
    }

    /**
     * @Route(
     *     path="/api/members/{id}",
     *     name="member",
     * )
     */
    public function getMember($id){
        $member = $this->getDoctrine()->getRepository(Members::class)->find($id);
        return $this->json($member);
    }
}
