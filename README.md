# test_kosmos

Projet de test technique Kosmos

**installation:**
- composer install

Vérifier les paramètres de connection à la base de données ("DATABASE_URL") dans le ficher .env
- Créer la base de données et les tables à l'aide des commandes:

1. php bin/console make:migration
2. php bin/console doctrine:migrations:migrate

- Démarrer le serveur à l'aide de la commande:

 symfony server:start